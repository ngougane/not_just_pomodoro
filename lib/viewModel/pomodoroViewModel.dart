// ignore_for_file: file_names

import 'package:not_just_pomodoro/model/timerModel.dart';
import 'package:not_just_pomodoro/utils/timer_utils.dart';

class PomodoroViewModel {
  //provide data in the view
  var timer = TimerModel();

  parseStringDurationToInt(String duration) {
    if (!isValideFormat(duration)) {
      return -1;
    } else {
      final splited = duration.split(":");
      int minutes = int.parse(splited[0]);
      int secondes = int.parse(splited[1]);
      int minutesToSecondes, durationInSecondes;
      minutesToSecondes = minutes * 60;
      durationInSecondes = minutesToSecondes + secondes;
      return durationInSecondes;
    }
  }
}
