import 'package:not_just_pomodoro/model/timerModel.dart';

final timer = TimerModel();

getStart() {
  return timer.startTimer;
}

isValideFormat(String duration) {
  RegExp exp = new RegExp(r'^[0-9]?\d:[0-6]\d$');
  return exp.hasMatch(duration);
}
