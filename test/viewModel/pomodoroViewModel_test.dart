// ignore_for_file: file_names

import 'package:flutter_test/flutter_test.dart';
import 'package:not_just_pomodoro/viewModel/pomodoroViewModel.dart';

main() {
  test('Test de calcul du timer en secondes', () {
    final pomodorro = PomodoroViewModel();
    expect(pomodorro.parseStringDurationToInt("25:00"), 1500);
    expect(pomodorro.parseStringDurationToInt("azer"), -1);
    expect(pomodorro.parseStringDurationToInt("-2:00"), -1);
    expect(pomodorro.parseStringDurationToInt(""), -1);
    expect(pomodorro.parseStringDurationToInt("01:-20"), -1);
  });
}
